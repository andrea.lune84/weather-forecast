## Setup
composer install\
php artisan migrate\
php artisan db:seed

## Fetch weather forecast manually
php artisan weather:fetch

## Run basic unit test
php artisan test

## Note
Request for this test was to create an API to pull the Weather Forecast for the inputted Date.\
Since it was not specified, I assumed the location was a required parameter for the endpoint call.\
Also, Openweathermap does not allow to query by date, I didn't implement a fallback call to the service in case the query won't return any data.

