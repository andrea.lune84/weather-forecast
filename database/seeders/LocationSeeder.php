<?php

namespace Database\Seeders;

use App\Models\Location;
use Illuminate\Database\Seeder;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = [
            ['name' => 'New York', 'lat' => 40.69, 'lng' => -74.26],
            ['name' => 'London', 'lat' => 51.52, 'lng' => -0.38],
            ['name' => 'Paris', 'lat' => 48.85, 'lng' => 2.27],
            ['name' => 'Berlin', 'lat' => 52.50, 'lng' => 13.14],
            ['name' => 'Tokyo', 'lat' => 35.50, 'lng' => 138.64]
        ];
        foreach($locations as $location) {
            Location::create($location);
        }
    }
}
