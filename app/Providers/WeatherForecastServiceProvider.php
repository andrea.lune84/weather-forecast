<?php

namespace App\Providers;

use Event;
use Illuminate\Support\ServiceProvider;

use App\Contracts\WeatherForecast;
use App\Services\OpenWeatherService;
use App\Events\WeatherForecastDataFetched;
use App\Listeners\StoreWeatherForecastData;

class WeatherForecastServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(WeatherForecast::class, OpenWeatherService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Event::listen(
            WeatherForecastDataFetched::class,
            [StoreWeatherForecastData::class, 'handle']
        );
    }
}
