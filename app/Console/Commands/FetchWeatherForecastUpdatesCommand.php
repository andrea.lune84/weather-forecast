<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\WeatherForecastFetch;
use App\Models\Location;

class FetchWeatherForecastUpdatesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch weather forecast updated';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach(Location::all() as $location) {
            WeatherForecastFetch::dispatch($location->id, $location->lat, $location->lng);
        }
    }
}
