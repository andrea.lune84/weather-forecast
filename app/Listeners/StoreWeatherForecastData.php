<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Events\WeatherForecastDataFetched;
use App\Models\Forecast;

class StoreWeatherForecastData
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(WeatherForecastDataFetched $event)
    {
        foreach($event->data as $day) {
            $date = date('Y-m-d', $day['dt']);
            Forecast::updateOrCreate(
                ['location_id' => $event->locationId, 'date' => $date],
                ['daily' => $day]
            );
        }
    }
}
