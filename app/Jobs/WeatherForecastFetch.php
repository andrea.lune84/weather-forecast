<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Contracts\WeatherForecast;

class WeatherForecastFetch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $locationId;

    private $lat;

    private $lng;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $locationId, float $lat, float $lng)
    {
        $this->locationId = $locationId;
        $this->lat = $lat;
        $this->lng = $lng;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(WeatherForecast $wf)
    {
        $wf->fetchDaily($this->locationId, $this->lat, $this->lng);
    }
}
