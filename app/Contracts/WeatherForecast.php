<?php

namespace App\Contracts;

interface WeatherForecast
{
    /**
     * Fetch daily weather forecast for the next 7 days
     *
     * @param  int $locationId
     * @param  float $lat
     * @param  float $lng
     * @return array
     */
    public function fetchDaily(int $locationId, float $lat, float $lng);
}
