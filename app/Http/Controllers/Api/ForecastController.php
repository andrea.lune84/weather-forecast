<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\DailyForecastRequest;

use App\Http\Resources\ForecastResource;
use App\Models\Forecast;


class ForecastController extends Controller
{
    public function getDaily(DailyForecastRequest $request)
    {
        $forecast = Forecast::where('location_id', $request->location)->where('date', $request->date)->first();
        if ($forecast) {
            return new ForecastResource($forecast);
        }
        else {
            return response()->json('Data unavailable', 500);
        }
    }
}
