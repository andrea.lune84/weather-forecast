<?php
namespace App\Services;

use App\Contracts\WeatherForecast;
use App\Events\WeatherForecastDataFetched;

class OpenWeatherService implements WeatherForecast
{
    private function _query(string $url, array $params = []) {
        // add api key
        $params[] = 'appId=' . config('services.openweather.key');
        // init curl object
        $ch = curl_init();
        // define options
        $optArray = array(
            CURLOPT_URL => $url . '?' . join('&', $params),
            CURLOPT_RETURNTRANSFER => true
        );
        // apply those options
        curl_setopt_array($ch, $optArray);
        // execute request and get response
        $res = curl_exec($ch);

        return json_decode($res, true);
    }

    public function fetchDaily(int $locationId, float $lat, float $lng)
    {
        $url = 'https://api.openweathermap.org/data/2.5/onecall';
        $params = [
            'lat=' . $lat,
            'lon=' . $lng,
            'exclude=current,minutely,hourly'
        ];
        $data = $this->_query($url, $params);

        if (isset($data['daily'])) {
            WeatherForecastDataFetched::dispatch($locationId, $data['daily']);
        }
        else {
            throw new \ErrorException('Daily weather foreacast service unavailable');
        }
    }
}
