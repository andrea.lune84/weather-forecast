<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\ForecastController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('forecast', function() {
    return response()->json('This endpoint accepts post requests only', 404);
});
Route::post('forecast', [ForecastController::class, 'getDaily'])->name('forecast.getDaily');
