<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Location;
use Carbon\Carbon;

class ForecastTest extends TestCase
{
    /**
     * A basic endpoint test
     *
     * @return void
     */
    public function testGetDailyForecast()
    {
        $data = [
            'location' => Location::first()->id,
            'date' => Carbon::now()->format('Y-m-d')
        ];

        $this->postJson(route('forecast.getDaily'), $data)
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'daily' => true,
                    'last_update' => true
                ]
            ]);
    }
}
